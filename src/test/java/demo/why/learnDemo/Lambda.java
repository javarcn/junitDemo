package demo.why.learnDemo;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class Lambda {
    @Test
    public void map() {
        Integer[] array = new Integer[]{1, 3, 6, 10, 233};
        List<Integer> newArray = Arrays.asList(array).stream().map(a -> a + 1)
                .collect(Collectors.toList());
        assertNotNull(newArray);
        assertEquals(5, newArray.size());
        assertEquals(Arrays.asList(2, 4, 7, 11, 234), newArray);
    }

    @Test
    public void reduce() {
        Integer[] array = new Integer[]{6, 2, 1, -7, 0, 10, -233};
        int sum = Arrays.asList(array).stream()
                .filter(a -> a > 0)
                .reduce((a, b) -> a + b)
                .get();
        assertEquals(19, sum);
    }
}
