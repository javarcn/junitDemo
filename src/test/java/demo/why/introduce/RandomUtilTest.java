package demo.why.introduce;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class RandomUtilTest {
    @Test
    public void generateNumber() {
        for (int i = 0; i < 100; i++) {
            String number = RandomUtil.generateNumber();
            assertTrue(null != number);
            assertEquals("数字字符串长度为4", 4, number.length());
            assertTrue("字符串，是纯数字字符串，取之范围[0,9999]", Integer.parseInt(number) >= 0 && Integer.parseInt(number) <= 9999);
        }
    }

    @Test
    public void generateNumber8() {
        for (int i = 0; i < 100; i++) {
            String number = RandomUtil.generateNumber8();
            assertTrue(null != number);
            assertEquals("数字字符串长度为8", 8, number.length());
            assertTrue("字符串，是纯数字字符串，取之范围[0,99999999]", Integer.parseInt(number) >= 0 && Integer.parseInt(number) <= 99999999);
        }
    }

    @Test
    public void generateNumberWithResultLengthParameter() {
        for (int i = 1; i < 100; i++) {
            String number = RandomUtil.generateNumber(i);
            assertTrue(null != number);
            assertEquals(i, number.length());
            Integer.parseInt(number);
        }
    }

    @Test
    public void generateShortNumber() {
        for (int i = 1; i < 10; i++) {
            String number = RandomUtil.generateShortNumber(i);
            assertTrue(null != number);
            Integer.parseInt(number);
            assertTrue(Integer.parseInt(number.substring(0, 1)) >= 3);
        }
    }
}
