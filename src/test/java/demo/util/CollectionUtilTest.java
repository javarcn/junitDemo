package demo.util;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

import static org.junit.Assert.*;

/**
 * @author buxianglong
 * @date 2018/11/01
 **/
public class CollectionUtilTest{
    @Test
    public void isEmpty() throws Exception{
        assertTrue(CollectionUtil.isEmpty(null));
        assertTrue(CollectionUtil.isEmpty(new ArrayList<Integer>()));
        assertTrue(CollectionUtil.isEmpty(new HashSet<String>()));

        assertFalse(CollectionUtil.isEmpty(Arrays.asList("Jack")));
    }

    @Test
    public void isNotEmpty() throws Exception{
        assertFalse(CollectionUtil.isNotEmpty(null));
        assertFalse(CollectionUtil.isNotEmpty(new ArrayList<Integer>()));
        assertFalse(CollectionUtil.isNotEmpty(new HashSet<String>()));

        assertTrue(CollectionUtil.isNotEmpty(Arrays.asList("Jack")));
    }

    @Test
    public void emptyWrap() throws Exception{
        assertNotNull(CollectionUtil.emptyWrap(null));
        assertEquals(0, CollectionUtil.emptyWrap(null).size());
    }

}