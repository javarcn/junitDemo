package demo.service;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author buxianglong
 * @date 2018/10/31
 **/
public class MakeNameServiceTest{
    @Test
    public void makeName() throws Exception{
        RandomNameService randomNameServiceMock = mock(RandomNameService.class);
        when(randomNameServiceMock.randomNickName()).thenReturn("芹菜");
        assertEquals("赵芹菜", new MakeNameService(randomNameServiceMock).makeName("赵"));
    }

}