package demo.basic;

import org.junit.Test;

import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.assertEquals;

/**
 * @author buxianglong
 * @date 2018/10/31
 **/
public class DateUtilTest{
    @Test
    public void testGetMonth(){
        assertEquals(11, DateUtil.getMonth());
    }

    @Test
    public void testGetMonthByDate(){
        Date date = buildDate(2018, Calendar.NOVEMBER, 1);
        assertEquals(11, DateUtil.getMonthByDate(date));
        date = buildDate(2018, Calendar.OCTOBER, 31);
        assertEquals(10, DateUtil.getMonthByDate(date));
    }

    /**
     * 辅助类：构建日期
     *
     * @param year  年
     * @param month 月
     * @param day   日
     * @return 日期
     */
    private Date buildDate(int year, int month, int day){
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);
        return calendar.getTime();
    }
}