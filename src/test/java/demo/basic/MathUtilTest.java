package demo.basic;

import demo.exception.BusinessException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * @author buxianglong
 * @date 2018/10/31
 **/
public class MathUtilTest{
    @Test(expected = ArithmeticException.class)
    public void divide() throws Exception{
        assertEquals(6, MathUtil.divide(12, 2));
        assertEquals(2, MathUtil.divide(5, 2));
        MathUtil.divide(5, 0);
    }

    @Test(expected = demo.exception.BusinessException.class)
    public void divide2() throws demo.exception.BusinessException{
        MathUtil.divide2(5, 0);
    }

    @Test
    public void divide3(){
        try{
            MathUtil.divide2(5, 0);
            fail("Expected an BusinessException to be thrown");
        }catch(BusinessException be){
            assertEquals("分母不能为0", be.getMessage());
        }
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void divide4() throws BusinessException{
        thrown.expect(BusinessException.class);
        thrown.expectMessage("分母不能为0");
        MathUtil.divide2(5, 0);
    }
}