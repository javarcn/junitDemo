package demo.service;

/**
 * 起名服务
 *
 * @author buxianglong
 * @date 2018/10/31
 **/
public class MakeNameService{
    private RandomNameService randomNameService;

    public MakeNameService(RandomNameService randomNameService){
        this.randomNameService = randomNameService;
    }

    /**
     * 根据姓氏起名
     *
     * @param firstName 姓氏
     * @return
     */
    public String makeName(String firstName){
        return firstName + randomNameService.randomNickName();
    }
}
