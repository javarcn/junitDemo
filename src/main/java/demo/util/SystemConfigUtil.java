package demo.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

/**
 * Created by chengkunxf@126.com on 2016-09-20.
 */
public class SystemConfigUtil {
    private static Properties properties;

    static {
        // 加载属性文件
        properties = new Properties();
        InputStream in = null;
        try {
            in = SystemConfigUtil.class.getClassLoader().getResourceAsStream("system.properties");
            properties.load(new InputStreamReader(in, "UTF-8"));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static String getProperties(String key) {
        return properties.getProperty(key);
    }

    public static String getProperties(String key, String defaultValue) {
        String value = properties.getProperty(key);
        return (null == value || "" == value || 0 == value.length()) ? defaultValue : value;
    }
}

