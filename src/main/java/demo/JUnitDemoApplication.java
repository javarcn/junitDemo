package demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/**
 * @author buxianglong
 * @date 2018/10/31
 **/
@ServletComponentScan
@SpringBootApplication
public class JUnitDemoApplication extends SpringBootServletInitializer{
    public static void main(String[] args){
        SpringApplication.run(JUnitDemoApplication.class, args);
    }
}
