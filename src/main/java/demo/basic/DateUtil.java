package demo.basic;

import java.util.Calendar;
import java.util.Date;

/**
 * @author buxianglong
 * @date 2018/10/31
 **/
public class DateUtil{
    /**
     * 获取当前月份
     *
     * @return 月份数
     */
    public static int getMonth(){
        Calendar calendar = Calendar.getInstance();
        return calendar.get(Calendar.MONTH) + 1;
    }

    /**
     * 获取当前月份
     *
     * @return 月份数
     */
    public static int getMonthByDate(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.MONTH) + 1;
    }
}
